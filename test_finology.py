from flask import Flask, request, render_template, jsonify
from flask_sqlalchemy import SQLAlchemy
from models import app, db, btn_pk, btn_sp3k
from suds.client import Client
import xmltodict


@app.route("/apis/btn-data-cosumer" , methods=['GET', 'POST'])
def GetDataBTNConsumer():
    # Get variable from http POST
    if request.method=='GET':
        return render_template("GetDataBTNConsumer.html")
    ap_RegNo = request.form['ap_RegNo']
    # Get SOAP Service via suds
    url = 'http://localip/ws_BTN/Service.asmx?WSDL'
    client = Client(url)
    # Execute SOAP
    xml = client.service.GetDataBTNConsumer(ap_RegNo)
    # Convert XML to dict
    res_dict = xmltodict.parse(xml)
    # Convert dict to JSON
    return jsonify(res_dict)

@app.route("/apis/btn-list-cosumer" , methods=['GET', 'POST'])
def GetListBTNConsumer():
    # Get variable from http POST
    if request.method=='GET':
        return render_template("GetListBTNConsumer.html")
    data = request.form['data']
    tanggal = request.form['tanggal']
    uid = request.form['uid']
    # Get SOAP Service via suds
    url = 'http://localip/ws_BTN/Service.asmx?WSDL'
    client = Client(url)
    # Execute SOAP
    xml = client.service.GetListBTNConsumer(data, tanggal, uid)
    # Convert XML to dict
    res_dict = xmltodict.parse(xml)
    # Convert dict to JSON
    return jsonify(res_dict)

@app.route("/apis/btn-list-cosumer-range" , methods=['GET', 'POST'])
def GetListBTNConsumerRange():
    # Get variable from http POST
    if request.method=='GET':
        return render_template("GetListBTNConsumerRange.html")
    data = request.form['data']
    tanggal_mulai = request.form['tanggal_mulai']
    tanggal_akhir = request.form['tanggal_akhir']
    uid = request.form['uid']
    # Get SOAP Service via suds
    url = 'http://localip/ws_BTN/Service.asmx?WSDL'
    client = Client(url)
    # Execute SOAP
    xml = client.service.GetListBTNConsumerRange(data, tanggal_mulai, tanggal_akhir, uid)
    # Convert XML to dict
    res_dict = xmltodict.parse(xml)
    # Convert dict to JSON
    return jsonify(res_dict)

@app.route('/')
def index():
    return render_template('index.html')

if __name__ == "__main__":
    app.run(host='0.0.0.0')